/* Теоретичні питання.
1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
setTimeout() встановлює паузу - час до запуску виконання коду, що прописаний в функції. Наприклда, якщо потрібно розпочати виконання коду через 10 секунд після загруження сторінки (або певної події).
setInterval() встановлює періодичність запущення коду, що прописаний в функції. Наприклад, якщо потрібно запускати одну й ту ж саму програму кожні n секунд.
Можливості setInterval() також повторює вкладений setTimeout(), але такий спосіб робить запуск коду більш гнучким (більш корректним).
2. Щоо станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Якщо в функцію setTimeout() передати нульову затримку, код в тілі callback функції виконається одразу після виконання всього коду. Тобто, без коермо встановлених затримок. Але й не одночасно з виконанням всього коду - одразу, але після нього.
3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
Без clearInterval() код в setInterval() буде лишатися в пам'яті навіть, якщо функція вже не потрібна. Тобто, код буде "важким", що напряму впливатиме на швидкість завантаження сторінок сайту.
*/

/* Практичне завдання.
При запуску програми на екрані має відображатись перша картинка. Через 3 секунди замість неї має бути показано друга картинка. Ще через 3 секунди – третя. Ще через 3 секунди – четверта. Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.

Необов'язкове завдання підвищеної складності
При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.
*/
let coll = document.querySelectorAll(".image-to-show");
let element = document.querySelector(".images-wrapper");
let father = document.querySelector(".images-wrapper");
let playButton = document.querySelector(".button-one");
let stopButton = document.querySelector(".button-two");
let slideIndex = 0;
let playing = true;

showSlides();
function showSlides() {
  if (playing) {
    let i;
    let slides = document.getElementsByClassName("image-to-show");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.visibility = "hidden";
    }
    slideIndex++;
    if (slideIndex > slides.length) {
      slideIndex = 1;
    }
    slides[slideIndex - 1].style.visibility = "";
    setTimeout(showSlides, 3000);
  }
}

function toggleSlideshowStart() {
  if (!playing) {
    playing = true;
    showSlides();
  }
}

function toggleSlideshowStop() {
  if (playing) {
    playing = false;
  }
}

playButton.addEventListener("click", toggleSlideshowStart);
stopButton.addEventListener("click", toggleSlideshowStop);
